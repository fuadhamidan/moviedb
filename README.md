
**Spesifikasi Movie Db:**
- Project structure by feature
- Architecture MVVM
- RxAndroid
- Butterknife
- Timber
- Retrofit
- Glide
- Data Binding
- REALM
- Gson


**Setup API KEY:**

Update API_KEY di class Cons.java. API KEY dapat diperoleh di https://www.themoviedb.org

`MOVIE_DB_API_KEY = "YOUR_API_KEY";`







**Screenshot:**


![Alt Text](http://i.imgur.com/bdxOgxZ.png)  ![Alt Text](http://i.imgur.com/632ERFs.png)  ![Alt Text](http://i.imgur.com/2Krs4ZK.png)

